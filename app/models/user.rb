class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.name = auth.info.name
      user.password = Devise.friendly_token[0,20]
      user.image = auth.info.image
      unless auth.extra.blank? or auth.extra.raw_info.blank?
        user.location = auth.extra.raw_info.location.name unless auth.extra.raw_info.location.blank?
        user.sex = auth.extra.raw_info.gender
      Rails.logger.debug("  ============== # {} My object:  ====== acao: " + "#{auth} ------ #{auth.extra.raw_info.gender}")
      end

    end
  end



  def self.sex_to_select
    [
      "Male.",
      "Female.",
      "I don't wanna say."
    ]
  end


end