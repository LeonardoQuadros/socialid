class PagesController < PublicController
  def home
    @get_info = params[:data]    
    params[:token].blank? ? (params[:token] = session[:token] unless session[:token].blank?) : nil
    unless params[:token].blank?
      uri = URI("https://api.socialidnow.com/v1/marketing/login/info?api_secret=921c4749711c4c794fb5d75bee1d97349330ca4e535640139b98e66acfee715a&token=#{params[:token]}")
      @net_http = Net::HTTP.get(uri)
      session[:token] = params[:token] unless params[:token].blank?
    end
  end

  def logout
  	session.delete(:token)
  end

end
