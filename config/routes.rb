Rails.application.routes.draw do

  #Espaço do Admin
  namespace :admin do


    controller :pages do
        get '/home' => :home
    end
    root 'pages#home'
  end

  devise_for :users, :controllers => { registrations: 'registrations',
                                        :omniauth_callbacks => "callbacks" }



  authenticated :user do
    root 'admin/pages#home', as: :authenticated_root
  end

  #Espaço Público
  controller :pages do
    get '/home' => :home
  	get '/logout' => :logout
  end 

  root 'pages#home'

end