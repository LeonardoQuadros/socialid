class ChangeDescriptionToUsers < ActiveRecord::Migration[5.0]
  def change
  	remove_column :users, :description
  	add_column :users, :description, :string
  end
end
