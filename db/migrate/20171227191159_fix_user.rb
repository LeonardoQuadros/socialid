class FixUser < ActiveRecord::Migration[5.0]
  def change
  	remove_column :users, :provider
  	remove_column :users, :uid
  	remove_column :users, :name
  	remove_column :users, :image
  	add_column :users, :provider, :string
  	add_column :users, :uid, :string
  	add_column :users, :name, :string
  	add_column :users, :image, :string
  end
end
