class ChangeAvatarToImagem < ActiveRecord::Migration[5.0]
  def change
  	rename_column :users, :avatar, :image
  	remove_column :users, :location
  	remove_column :users, :sex
  	add_column :users, :location, :string
  	add_column :users, :sex, :string
  end
end
